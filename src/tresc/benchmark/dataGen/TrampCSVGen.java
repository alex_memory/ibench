package tresc.benchmark.dataGen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.vagabond.util.LoggerUtil;

import smark.support.SMarkElement;
import tresc.benchmark.Configuration;
import vtools.dataModel.schema.Schema;

public class TrampCSVGen extends ToXDataGenerator {

	// private static final String XML_TO_CSV_XSLT_TEMPLATE_XML =
	// 		"resource/xmlToCSV_XSLT_template.xml";
	private static final String XML_TO_CSV_XSLT_TEMPLATE_XML =
			"/resource/xmlToCSV_XSLT_template.xml";

	Logger log = Logger.getLogger(TrampCSVGen.class);

	private String templateXSLT;

	static {
		System.setProperty("javax.xml.transform.TransformerFactory",
				"net.sf.saxon.TransformerFactoryImpl");
	}

	public TrampCSVGen(Configuration config) {
		super(config);
	}

	public TrampCSVGen(Schema schema, Configuration config) {
		super(schema, config);
	}

	@Override
	protected void initFromConfig() {
		super.initFromConfig();
	}

	private void readTemplate() throws IOException {
		StringBuffer result = new StringBuffer();
		// BufferedReader in =
		// 		new BufferedReader(new FileReader(XML_TO_CSV_XSLT_TEMPLATE_XML));
		BufferedReader in =
                    new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(XML_TO_CSV_XSLT_TEMPLATE_XML)));

		while (in.ready()) {
			result.append(in.readLine() + "\n");
		}
		in.close();

		templateXSLT = result.toString();
	}

	@Override
	public void generateData() throws Exception {
		readTemplate();
		super.generateData();
		xsltToCsv();
	}

	private void xsltToCsv() {
                System.out.println("TrampCSVGen Configuration.getInstancePathPrefix() "+Configuration.getInstancePathPrefix());
                System.out.println("TrampCSVGen instanceXMLFile "+instanceXMLFile);
                System.out.println("TrampCSVGen outputPath "+outputPath);
		File instFile = new File(Configuration.getInstancePathPrefix(), instanceXMLFile);
		File outFile;
                System.out.println("TrampCSVGen instFile "+instFile);
                System.out.println("TrampCSVGen schema.size() "+schema.size());

		// create one CSV file for each relation
		for (int i = 0; i < schema.size(); i++) {
                        System.out.println("TrampCSVGen schema.getSubElement(i) "+schema.getSubElement(i));
			SMarkElement rootSetElt = (SMarkElement) schema.getSubElement(i);
			String relName = rootSetElt.getLabel();
			String xsltScript = templateXSLT.replace("$RELNAME$", relName);
                        if (!isSchemaSource) {
                            xsltScript = xsltScript .replace("Source/", "Target/");
                        }
                        System.out.println("TrampCSVGen xsltScript "+xsltScript);
			outFile = new File(outputPath, relName + ".csv");
			if (log.isDebugEnabled()) {log.debug("use XSLT script:\n" + xsltScript);};
			
                        System.out.println("TrampCSVGen.transform");
			transform(xsltScript, instFile, outFile);
                        System.out.println("TrampCSVGen.transform done");
		}
	}

	private void transform(String script, File instFile, File outFile) {
		TransformerFactory tFactory = TransformerFactory.newInstance();
		try {
			Transformer transformer =
					tFactory.newTransformer(new StreamSource(new StringReader(
							script)));

                        System.out.println("TrampCSVGen.transformer.transform");
			transformer.transform(new StreamSource(instFile), new StreamResult(
					outFile));
                        System.out.println("TrampCSVGen.transformer.transform out");
		}
		catch (Exception e) {
                        e.printStackTrace();
			LoggerUtil.logException(e, log);
		}
	}
}
