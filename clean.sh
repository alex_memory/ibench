#!/bin/sh

# step 1 - clean iBench output
rm out0/*

# step 2 - clean Clio/MapMerge output
rm ~/millercode/Clio_MapMerge/branches/Radu/com.ibm.clio.gui/S*sql

# step 3 - clean from Tramp
rm ~/millercode/trampExGen/branches/2525-Mehrnaz/S*sql
rm ~/millercode/trampExGen/branches/2525-Mehrnaz/Schemas*xml
